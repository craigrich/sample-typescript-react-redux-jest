import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import apiWithRetry from 'lib/apiWithRetry';
import { actions as noteActions } from 'modules/notes';

import reducer, {
  actions,
  getOrdersState,
  getOrderById,
  initialState,
  getOrdersLoading,
  getOrdersError,
  fetchOrders
} from './orders';

import { Order } from './types';

const mockStore = configureMockStore([thunk]);

jest.mock('lib/apiWithRetry');

describe('Orders', () => {
  describe('Reducer', () => {
    test('When initialised, return initial state', () => {
      expect(reducer(undefined, {} as any)).toBe(initialState);
    });

    test('When a fetch start action is fired, return loading state', () => {
      const nextState = reducer(initialState, actions.fetchStart());
      const rootState = { orders: nextState } as any;
      expect(getOrdersState(rootState).loading).toEqual(true);
      expect(getOrderById(1234)(rootState)).toEqual(undefined);
    });

    test('When a fetch success action is fired, return a list a orders', () => {
      const exampleOrder = { id: 1234 } as Order;
      const nextState = reducer(
        initialState,
        actions.fetchSuccess([exampleOrder])
      );
      const rootState = { orders: nextState } as any;
      expect(getOrdersState(rootState).loading).toEqual(false);
      expect(getOrderById(1234)(rootState)).toEqual(exampleOrder);
    });

    test('When a fetch success action is fired, return a list a orders', () => {
      const exampleOrder = { id: 1234 } as Order;
      const nextState = reducer(
        initialState,
        actions.fetchSuccess([exampleOrder])
      );
      const rootState = { orders: nextState } as any;
      expect(getOrdersState(rootState).loading).toEqual(false);
      expect(getOrderById(1234)(rootState)).toEqual(exampleOrder);
    });

    test('When a fetch error action is fired, return valid error object', () => {
      const exampleError = new Error('failed to load');
      const nextState = reducer(initialState, actions.fetchError(exampleError));
      const rootState = { orders: nextState } as any;
      expect(getOrdersLoading(rootState)).toEqual(false);
      expect(getOrdersError(rootState)).toEqual(exampleError);
    });
  });

  describe('Thunks', () => {
    describe('fetchOrders', () => {
      test('When initialised, return initial state', async () => {
        expect(reducer(undefined, {} as any)).toBe(initialState);
        const store = mockStore(initialState);
        const responsePayload = {
          orders: [
            {
              id: 1,
              productionId: 'abc/111'
            },
            {
              id: 2,
              productionId: 'abc/222'
            }
          ]
        };
        apiWithRetry.mockResolvedValueOnce(responsePayload);
        await store.dispatch(fetchOrders({}));
        const expectedActions = [
          actions.fetchStart(),
          actions.fetchSuccess(responsePayload.orders),
          noteActions.fetchStart()
        ];
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
