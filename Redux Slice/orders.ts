import { createSlice, PayloadAction } from 'redux-starter-kit';
import { Order } from './types';
import { RootState } from 'modules';
import { AppThunk } from 'createStore';
import apiWithRetry from 'lib/apiWithRetry';

interface OrdersState {
  data: { [orderId: number]: Order };
  loading: boolean;
  updating: boolean;
  error: Error;
}

export const initialState: OrdersState = {
  data: {},
  loading: true,
  updating: false,
  error: undefined
};

const ordersSlice = createSlice({
  name: 'orders',
  initialState,
  reducers: {
    fetchStart: state => {
      state.loading = true;
      state.data = {};
    },
    fetchSuccess: (state, action: PayloadAction<Order[]>) => {
      state.loading = false;
      action.payload.forEach(item => {
        state.data[item.id] = item;
      });
    },
    fetchError: (state, action: PayloadAction<Error>) => {
      state.loading = false;
      state.error = action.payload;
    },
    updateStart: state => {
      state.updating = true;
    },
    updateSuccess: (state, action: PayloadAction<Order[]>) => {
      state.updating = false;
      action.payload.forEach(item => {
        state.data[item.id] = item;
      });
    },
    updateError: state => {
      state.updating = false;
    }
  }
});

export default ordersSlice.reducer;

export const actions = ordersSlice.actions;

export const getOrdersState = (state: RootState) => state.orders;

export const getOrders = (state: RootState): Order[] =>
  Object.values(getOrdersState(state).data);

export const getOrderById = orderId => (state: RootState): Order =>
  state.orders.data[orderId];

export const fetchOrders = (query): AppThunk => async dispatch => {
  try {
    dispatch(actions.fetchStart());
    const { orders } = await apiWithRetry('fetchOrders', query);
    dispatch(actions.fetchSuccess(orders));
  } catch (err) {
    dispatch(actions.fetchError(err));
  }
};

export const updateOrders = (orders: Order[]): AppThunk => async dispatch => {
  try {
    dispatch(actions.updateStart());
    const updatedOrders = await Promise.all(
      orders.map(
        async order =>
          await apiWithRetry('updateOrder', {
            orderId: order.id,
            timestamp: order.systemPeriod.start,
            payload: {
              order: order
            }
          })
      )
    );
    dispatch(actions.updateSuccess(updatedOrders));
  } catch (err) {
    console.error('Failed To Update', err);
    dispatch(actions.updateError());
  }
};
