export enum OrderState {
  WAITING = 'WAITING',
  PROCESSING = 'PROCESSING',
  ARCHIVED = 'ARCHIVED'
}

export enum AssetType {
  VIDEO = 'VIDEO'
}

export interface Order {
  id: number;
  productionId: string;
  state: OrderState;
  ingestHouse: string;
  supplierId?: string;
  assetType: AssetType;
  requiredBy?: string;
  dueIn?: string;
  systemPeriod: {
    start: string;
    end?: string;
  };
}
