import React from 'react';
import { render } from '@testing-library/react';
import ProductionInfo from '../ProductionInfo';
import useFetchData from 'lib/useFetchData';
import '@testing-library/jest-dom/extend-expect';

jest.mock('lib/useFetchData');

const exampleProd = {
  programmeTitle: 'Example Production Title',
  seriesTitle: 'Series 1'
};

describe('ProductionInfo', () => {
  test('When fetching a production, show a loading indicator', () => {
    useFetchData.mockReturnValue({
      isLoading: true,
      hasError: false,
      data: exampleProd
    });
    const { queryByText } = render(<ProductionInfo prodId="123" />);
    expect(queryByText('Example Production Title')).not.toBeInTheDocument();
  });

  test('When an error has occured fetching a production, show an error message', () => {
    useFetchData.mockReturnValue({
      isLoading: false,
      hasError: true,
      data: exampleProd
    });
    const { queryByText } = render(<ProductionInfo prodId="123" />);
    expect(queryByText('Example Production Title')).not.toBeInTheDocument();
    expect(queryByText('Error')).toBeInTheDocument();
  });

  test('When a production is fetched, show production info', () => {
    useFetchData.mockReturnValue({
      isLoading: false,
      hasError: false,
      data: exampleProd
    });
    const { getByText } = render(<ProductionInfo prodId="123" />);
    expect(getByText('Example Production Title')).toBeInTheDocument();
  });
});
