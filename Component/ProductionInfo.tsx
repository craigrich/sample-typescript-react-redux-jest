import React, { FC } from 'react';

import { Order } from 'modules/orders';
import useFetchData from 'lib/useFetchData';
import { ContentLoader } from '@itv-ct/content-technology-ui-styleguide';

interface Props {
  prodId: Order['productionId'];
}

const ProductionInfo: FC<Props> = ({ prodId }) => {
  const { isLoading, data, hasError } = useFetchData('fetchProduction', prodId);

  if (isLoading) {
    return (
      <>
        <ContentLoader width={150} />
        <div className="sub-text">
          <ContentLoader width={90} />
        </div>
      </>
    );
  }

  if (hasError) {
    return (
      <div className="text-truncate">
        Server Error. Please contact Craft Support
      </div>
    );
  }

  return (
    <>
      <div className="text-truncate">{data.programmeTitle}</div>
      <div className="sub-text">{data.seriesTitle}</div>
    </>
  );
};

export default ProductionInfo;
