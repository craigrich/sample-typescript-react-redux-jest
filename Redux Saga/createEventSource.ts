import { eventChannel } from 'redux-saga';

export default function createSource(url) {
  const source = new EventSource(url);
  return eventChannel(emit => {
    let eventQueue: any[] = [];

    //for every 1000ms, emit any new events from the queue
    const interval = setInterval(() => {
      if (eventQueue.length) {
        emit(eventQueue);
        eventQueue = [];
      }
    }, 1000);

    source.onmessage = event => eventQueue.push(JSON.parse(event.data));

    return () => {
      clearInterval(interval);
      source.close();
    };
  });
}
