import { call, put, takeEvery, cancel, all, select } from 'redux-saga/effects';
import createEventSource from 'sagas/utils/createEventSource';
import apiWithRetry from 'utils/apiWithRetry';

export default function* pollRequests() {
  const eventSource = createEventSource(
    `/events?after=${new Date().toISOString()}`
  );
  const watcherInstance = yield takeEvery(eventSource, processEvents);
  yield takeEvery(
    '@@router/LOCATION_CHANGE',
    cancelPolling,
    watcherInstance,
    eventSource
  );
}

function* processEvents(events) {
  try {
    const newRequests = yield all(
      events.map(msg =>
        call(apiWithRetry, 'fetchAssetRequest', msg.assetRequestId)
      )
    );
    yield put(actions.updateSuccess(newRequests));
  } catch (err) {
    console.error('Failed to process Event - ', err);
    yield put(actions.updateError(err));
  }
}

function* cancelPolling(watcherInstance, eventSource) {
  eventSource.close();
  yield cancel(watcherInstance);
}
