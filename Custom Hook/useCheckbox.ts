import React from 'react';

function useCheckbox<T>() {
  const [activeCheckboxes, setCheckboxes] = React.useState<T[]>([]);

  const toggleCheckbox = (id: T) => {
    const updatedCheckboxes = activeCheckboxes.includes(id)
      ? activeCheckboxes.filter(activeId => activeId !== id)
      : activeCheckboxes.concat(id);
    setCheckboxes(updatedCheckboxes);
  };

  const clearActiveCheckboxes = () => setCheckboxes([]);

  return { activeCheckboxes, toggleCheckbox, clearActiveCheckboxes };
}

export default useCheckbox;
