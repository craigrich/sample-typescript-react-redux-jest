import useCheckbox from '../useCheckbox';
import { renderHook, act } from '@testing-library/react-hooks';

describe('useCheckbox', () => {
  test('When checking a single checkbox, return a list containing that checkbox ID', () => {
    const { result } = renderHook(() => useCheckbox());
    act(() => result.current.toggleCheckbox('123'));
    expect(result.current.activeCheckboxes).toEqual(['123']);
  });

  test('When checking mutilpe checkboxs, return a list containing that checkbox ID', () => {
    const { result } = renderHook(() => useCheckbox());
    act(() => result.current.toggleCheckbox(['123', '456']));
    expect(result.current.activeCheckboxes).toEqual(['123', '456']);
  });

  test('When checkbox is unchecked, return a list not containing that checkbox ID', () => {
    const { result } = renderHook(() => useCheckbox());
    act(() => result.current.toggleCheckbox(['123', '456']));
    act(() => result.current.toggleCheckbox('123'));
    expect(result.current.activeCheckboxes).toEqual(['456']);
  });

  test('When clearActiveCheckboxes is called, return a list with no IDs', () => {
    const { result } = renderHook(() => useCheckbox());
    act(() => result.current.toggleCheckbox(['123', '456']));
    act(() => result.current.clearActiveCheckboxes());
    expect(result.current.activeCheckboxes).toEqual([]);
  });
});
